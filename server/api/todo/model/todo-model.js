"use strict";

const mongoose = require('mongoose');

const _todoSchema = {
    title: {type: String, required: true, trim: true},
    section: {type: String, required: true, trim: true},
    description: {type: String, trim: true},
    tags: [{type: String, trim: true}],
    variations: [{type: String, trim: true}],
    functionalities: [{type: String, trim: true}],
    createdAt: {type: Date, default: Date.now}
}

module.exports = mongoose.Schema(_todoSchema);
