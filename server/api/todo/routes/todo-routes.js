"use strict";

const TodoController = require('../controller/todo-controller');

module.exports = class TodoRoutes {
    static init(router) {
      router
        .route('/api/features')
        .get(TodoController.getAll)
        .post(TodoController.createTodo);

      router
        .route('/api/features/:id')
        .get(TodoController.getOne)
        .post(TodoController.updateOne)
        .delete(TodoController.deleteTodo);
    }
}
