;(function(angular) {
  "use strict";

  angular
    .module('myAwesomeApp')
    .controller('featureListController', ['$log', 'Todo', 'TodoDAO', '$routeParams', function($log, Todo, TodoDAO, $routeParams) {
      var self = this;
      
      self.newFeature = new Todo();
      self.newSection = null;
      self.features = [];
      self.features_by_section = {};

      self.createTodo = function(todo) {
        var _onSuccess = function(newTodo) {
          self.features.push(newTodo);
          self.newFeature = new Todo();
        };

        TodoDAO
          .createTodo(todo)
          .then(_onSuccess)
          .catch($log.error);
      };

      self.addFeature = function() {
        var _onSuccess = function(newTodo) {
          self.features.push(newTodo);
          self.todo = new Todo();
        };

        TodoDAO
          .createTodo(self.newFeature)
          .then(_onSuccess)
          .catch($log.error);
      };

      self.deleteFeature = function(id) {
        if( confirm("Are you sure you want to delete that feature?") ){
          TodoDAO
            .deleteTodo(id)
            .then(_getAll)
            .catch($log.error);
        }
      }

      self.addSection = function(){
        self.featuresBySection[self.newSection] = [];
        self.newSection = '';
      }

      function _getFeaturesBySection(features){
        var result = {};
        for(var i = 0 ; i < features.length ; i ++){
          var feature = features[i];
          if( !result.hasOwnProperty(feature.section) ){
            result[feature.section] = [feature];
          } else {
            result[feature.section].push(feature);
          }
        }
        return result;
      }

      var _getAll = function() {
        var _onSuccess = function(features) {
          self.featuresBySection = _getFeaturesBySection(features);
          return self.features = features;
        };

        return TodoDAO
          .getAll()
          .then(_onSuccess)
          .catch($log.error);
      }

      _getAll();
    }]);
}(window.angular));
