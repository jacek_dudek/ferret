;(function(angular) {
  "use strict";

  angular
    .module('myAwesomeApp')
    .directive('featureListContainer', function() {

      var controller = [ '$scope', '$log', 'TodoDAO', 'Todo', function($scope, $log, TodoDAO, Todo){

        function _init(){
          $scope.newFeature = new Todo();
          $scope.newFeature.section = $scope.section;
          if(!$scope.features){
            $scope.features = [];
          }
        }

        $scope.addFeature = function() {
          var _onSuccess = function(newTodo) {
            $scope.features.push(newTodo);
            _init();
          };

          TodoDAO
            .createTodo($scope.newFeature)
            .then(_onSuccess)
            .catch($log.error);
        };


        $scope.deleteFeature = function(id) {
          function removeOnSuccess(){
            for (var i = 0; i < $scope.features.length; i++){
              if ($scope.features[i]._id === id) { 
                  $scope.features.splice(i, 1);
                  break;
              }
            }
          }
          if( confirm("Are you sure you want to delete that feature?") ){
            TodoDAO
              .deleteTodo(id)
              .then(removeOnSuccess)
              .catch($log.error);
          }
        }

        _init();

      }];

      return {
        restrict: 'E',
        transclude: true,
        scope: {
          section: '=',
          features: '='
        },
        templateUrl: 'todo/templates/featureListContainer.html',
        controller: controller
      };
    })

}(window.angular));
