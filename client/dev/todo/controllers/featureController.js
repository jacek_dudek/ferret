;(function(angular) {
  "use strict";

  angular
    .module('myAwesomeApp')
    .controller('featureController', ['$location', '$log', 'Todo', 'TodoDAO', '$routeParams', function($location, $log, Todo, TodoDAO, $routeParams) {
      var self = this;

      self.variation = null;
      function _updateFeature(preUpdated, onSuccess){
        TodoDAO
          .updateOne(preUpdated)
          .then(onSuccess)
          .catch($log.error);
      }

      self.addVariation = function(){

        var _copy = angular.copy(self.feature);
        _copy.variations.push(self.variation);

        _updateFeature(_copy, function(){
          self.feature = _copy;
          self.variation=null;
        });
      }

      self.createTodo = function(todo) {
        var _onSuccess = function(newTodo) {
          self.features.push(newTodo);
          self.todo = new Todo();
        };

        TodoDAO
          .createTodo(todo)
          .then(_onSuccess)
          .catch($log.error);
      };

      self.deleteTodo = function(id) {
        TodoDAO
          .deleteTodo(id)
          .then(_getAll)
          .catch($log.error);
      }

      self.save = function() {
        var _copy = angular.copy(self.feature);
        _updateFeature(_copy, function(){
          self.feature = _copy;
        });
      }

      var _getOne = function(id) {
        var _onSuccess = function(feature) {
          return self.feature = feature;
        };

        return TodoDAO
          .getOne(id)
          .then(_onSuccess)
          .catch($log.error);
      }

      if(!$routeParams.slug){
        alert("That event does not exist.");
        $location.path("/");
      }
      _getOne($routeParams.slug);
    }]);
}(window.angular));
