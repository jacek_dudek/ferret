;(function(angular) {
  "use strict";

  angular
    .module('myAwesomeApp')
    .factory('Todo', [function() {
      var Todo = function(todo) {
        this.title = null;
        this.section = null;
        this.description = null;
        this.tags = [];
        this.functionalities = [];
        this.variations = [];

        angular.extend(this, todo);
      }

      var MIN_ACCEPTED_LENGTH = 3;

      Todo.prototype.isValid = function() {
        var _isDefined = angular.isDefined(this.title) && angular.isDefined(this.section);
        var _isString = angular.isString(this.title) && angular.isString(this.section);
        var _isBigEnough = (_isDefined && _isString) ? this.title.length >= MIN_ACCEPTED_LENGTH : false;

        return _isDefined && _isString && _isBigEnough;
      }

      return Todo;
    }]);

}(window.angular));
