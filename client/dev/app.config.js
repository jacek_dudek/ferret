;(function(angular) {
  "use strict";

  angular
    .module('myAwesomeApp')
    .config(['$routeProvider', function($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'todo/templates/featureList.html',
          controller: 'featureListController',
          controllerAs: 'featureListCtrl'
        })
        .when('/features/:slug', {
          templateUrl: 'todo/templates/feature.html',
          controller: 'featureController',
          controllerAs: 'featureCtrl'
        })
        .otherwise({redirectTo: '/'});
    }])
    .config(['$locationProvider', function($locationProvider) {
      $locationProvider.html5Mode(true);
    }]);
}(window.angular));
